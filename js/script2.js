$(document).ready(function() {



	$('.menu-link').bigSlide({
		push: '.warp',
		side: 'right',
	});


	$(window).scroll(function() {    
		var scroll = $(window).scrollTop();

		if (scroll >= 100) {
			$("#topheader").addClass("scrolling");
		} else {
			$("#topheader").removeClass("scrolling");
		}
	});


	$('.techfocus').slick({
		dots: false,
		arrows: false,
		infinite: true,
		autoplay: true,
		autoplaySpeed: 2000,
		slidesToShow: 5,
		slidesToScroll: 3,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 3,
					infinite: true,
					dots: false
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					arrows: false,
					dots: false
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					arrows: false,
					dots: false
				}
			}
		]

	});



});



/*Home Form*/
$('#sdna_home_contact').submit(function(e) {
	e.preventDefault();

	$(this).on('valid', function() {

		$.ajax({
			url: 'processne.php',
			type: 'POST',
			data: {
				name: $("input[name='name']").val(),
				subject: $("input[name='subject']").val(),
				email: $("input[name='email']").val(),
				message: $("textarea[name='message']").val(),
			},
			dataType: 'json',
			success: function(data) {
				if (data == 1) {
					$('.sending').html('<h2>Sent Successfully Thank You!</h2>');
					$('#sdna_home_contact').fadeOut(800);

					$("#sdna_home_contact input:not(#submit)").val('');
					$('#sdna_home_contact textarea').val('');

					txt_name = null;
				} else alert("error sending");
			},
			error: function(err) {
				alert("error sending");
			}
		});
	});
});
/*End Home Form*/


/*Contact Form*/
$('#fm_sdna_ctnt').submit(function(e) {
	e.preventDefault();

	$(this).on('valid', function() {

		$.ajax({
			url: 'processwo.php',
			type: 'POST',
			data: {
				name: $("input[name='name']").val(),
				email: $("input[name='email']").val(),
				company: $("input[name='company']").val(),
				subject: $("input[name='subject']").val(),
				message: $("textarea[name='message']").val(),
			},
			dataType: 'json',
			success: function(data) {
				if (data == 1) {
					$('.sending').html('<h2>Thank You, We will get back to you soon</h2>');
					$('#fm_sdna_ctnt').fadeOut(800);

					$("#fm_sdna_ctnt input:not(#submit)").val('');
					$('#fm_sdna_ctnt textarea').val('');

					txt_name = null;
				} else alert("error sending");
			},
			error: function(err) {
				alert("error sending");
			}
		});
	});
});
/*End Contact Form*/

/*Google Maps v3*/

function initialize() {
	var myLatlng = new google.maps.LatLng(51.610812, -0.178160);
	var mapOptions = {
		center: myLatlng,
		zoom:13,
		mapTypeId: google.maps.MapTypeId.ROADMAP,styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"visibility":"simplified"},{"hue":"#0066ff"},{"saturation":74},{"lightness":100}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"off"},{"weight":0.6},{"saturation":-85},{"lightness":61}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"simplified"},{"color":"#5f94ff"},{"lightness":26},{"gamma":5.86}]}]
	}
	var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

	var markerImage = 'img/marker.png';

	var marker = new google.maps.Marker({
		position: myLatlng,
		map: map,
		icon: markerImage
	});
}

google.maps.event.addDomListener(window, 'load', initialize);

/*End Google Maps v3*/




